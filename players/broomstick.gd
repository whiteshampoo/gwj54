extends CharacterBody3D

const PITCH: Vector3 = Vector3.RIGHT
const ROLL: Vector3 = Vector3.BACK
const YAW: Vector3 = Vector3.DOWN

#@export var speed = 0.5

@export var speed_acceleration: float = 1.0
@export var speed_pitch: float = 0.05
@export var speed_roll: float = 0.05
@export var speed_yaw: float = 0.05


func _physics_process(delta: float) -> void:

	var acceleration: float = Input.get_axis("decelerate", "accelerate")
	var pitch: float = Input.get_axis("pitch_up", "pitch_down")
	var roll: float = Input.get_axis("roll_left", "roll_right")
	var yaw: float = Input.get_axis("yaw_left", "yaw_right")
	
	rotate(transform.basis.x, pitch / TAU * speed_pitch)
	rotate(transform.basis.z, roll / TAU * speed_roll)
	rotate(transform.basis.y, yaw / TAU * -speed_yaw)
	
	var speed: float = velocity.length() + acceleration * delta
	var direction: Vector3 = velocity.normalized()
	velocity = lerp(direction, transform.basis.z, delta) * speed
	
	move_and_slide()
