@tool
extends Path2D


@export var _update: bool = false:
	set(__):
		make_new()

@export var level_seed: int = 0
@export var radius: float = 100.0
@export var points: int = 100
@export var curve_width: float = 25.0
@export var noise_round: FastNoiseLite = FastNoiseLite.new()


func _ready() -> void:
	make_new()


func make_new() -> void:
	curve.clear_points()
	noise_round.seed = level_seed
	var angle: float = TAU / float(points)
	for point in points:
		var current_angle = point * angle
		var waypoint: Vector2 = Vector2.ZERO
		waypoint.x = radius + get_random_height(noise_round, current_angle) * curve_width
		waypoint.y = 0.0
		curve.add_point(waypoint.rotated(point * angle))


func get_random_height(noise: Noise, angle: float, offset: float = 100.0) -> float:
	var height_1: float = noise.get_noise_2d(angle, -offset)
	var height_2: float = noise.get_noise_2d(angle, offset)
	return height_1 * sin(angle) + height_2 * sin(angle + PI)

