@tool
extends Node2D

@export var _update: bool = false:
	set(__):
		generate()
@export var uva_textures: Array[GradientTexture2D]
@export var uva_texture: ImageTexture = ImageTexture.new()

@onready var edge_1: Path2D = $Edge1
@onready var edge_2: Path2D = $Edge2
@onready var road: Polygon2D = $Road

func generate() -> void:
	generate_uva_texture()
	var points: int = edge_1.points
	assert(points == edge_2.points)
	road.texture = uva_texture
	var polygon: PackedVector2Array = PackedVector2Array()
	var polygons: Array = Array()
	var uv: PackedVector2Array = PackedVector2Array()
	for i in points:
		polygon.append(edge_1.curve.samplef(i))
		polygon.append(edge_2.curve.samplef(i))
		if not i % 2:
			uv.append(Vector2.ZERO * 64)
			uv.append(Vector2.RIGHT * 64)
		else:
			uv.append(Vector2.DOWN * 64)
			uv.append(Vector2.RIGHT * 64 + Vector2.DOWN * 64)
		var poly: PackedInt32Array = PackedInt32Array()
		poly.append(i * 2)
		poly.append((i * 2) + 1)
		poly.append(((i * 2) + 2) % (points * 2))
		polygons.append(poly)
		poly = PackedInt32Array()
		poly.append(((i * 2) + 2) % (points * 2))
		poly.append(((i * 2) + 3) % (points * 2))
		poly.append((i * 2) + 1)
		polygons.append(poly)
	
	road.polygon = polygon
	road.polygons = polygons
	road.uv = uv
		


func generate_uva_texture() -> void:
	assert(uva_textures.size() == 3)
	var width: int = uva_textures[0].get_width()
	var height: int = uva_textures[0].get_height()
	var tex_img: Image = Image.create(width, height, false, Image.FORMAT_RGBA8)
	tex_img.fill(Color.BLACK)
	var imgs: Array = Array()
	for i in uva_textures.size():
		var image: Image = uva_textures[i].get_image()
		image.convert(Image.FORMAT_RGBA8)
		assert(width == image.get_width())
		assert(height == image.get_height())
		imgs.append(image)
	var pixel: Color = Color.BLACK
	for x in width:
		for y in height:
			pixel = Color.BLACK
			for img in imgs:
				pixel += img.get_pixel(x, y)
			tex_img.set_pixel(x, y, pixel)
	uva_texture.set_image(tex_img)
