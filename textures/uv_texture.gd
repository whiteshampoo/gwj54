@tool
class_name CoverageShader
extends ShaderMaterial

const ALBEDO: String = "Color.jpg"
const NORMAL: String = "NormalGL.jpg"
const AMBIENT: String = "AmbientOcclusion.jpg"
#const DISPLACE: String = "Displacement.jpg"
const ROUGH: String = "Roughness.jpg"

const REPEATS: int = 2

const SHADER: Shader = preload("res://textures/coverage.gdshader")

@warning_ignore("unused_private_class_variable")
@export var __update: bool = false:
	set(__):
		_update()

@export var mipmaps: bool = true
@export_dir var texture_directory: String = ""

func _init() -> void:
	shader = SHADER
	call_deferred("_update")
	

func _update() -> void:
	if not DirAccess.dir_exists_absolute(texture_directory):
		return
	var dir: String = texture_directory + "/%s"
	set_shader_parameter("albedo", load(dir % ALBEDO))
	set_shader_parameter("ambient_occlusion", load(dir % AMBIENT))
	set_shader_parameter("normal", load(dir % NORMAL))
	set_shader_parameter("roughness", load(dir % ROUGH))
