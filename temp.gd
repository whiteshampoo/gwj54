@tool
extends MeshInstance3D

@warning_ignore("unused_private_class_variable")
@export var _test: bool = false:
	set(__):
		bla()

@export var noise: FastNoiseLite = FastNoiseLite.new()

func _ready() -> void:
	bla()

func bla() -> void:
	var points: PackedFloat32Array = PackedFloat32Array()
	var w: int = 256
	var h: int = 256
	#points.resize(w * h)
	for x in w:
		for y in h:
			points.append(noise.get_noise_2d(x, y) * 20.0)
	var st: SurfaceTool = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	for point in points.size():
		var a1: int = calc_point(point, Vector2i.RIGHT, w, h)
		var a2: int = calc_point(point, Vector2i.DOWN, w, h)
		var b1: int = calc_point(point, Vector2i.LEFT, w, h)
		var b2: int = calc_point(point, Vector2i.UP, w, h)
		add_vertex(st, points, point, a1, a2, w, h)
		add_vertex(st, points, point, b1, b2, w, h)
	st.generate_normals()
	mesh = st.commit()


func add_vertex(st: SurfaceTool, points: PackedFloat32Array, a: int, b: int, c: int, w: int, h: int) -> void:
	#prints(a, b, c)
	if a < 0 or b < 0 or c < 0:
		return
	#print("ok")
	for vertex in [a, b, c]:
		var coord: Vector2i = calc_coordinates(vertex, w) 
		var height: float = points[vertex]
		var v: Vector3 = Vector3(coord.x, height, coord.y)
		st.set_uv(Vector2(coord.x / float(w), coord.y / float(h)))
		st.add_vertex(v)


func calc_point(point: int, offset: Vector2i, w: int, h: int) -> int:
	var coord: Vector2i = calc_coordinates(point, w)
	return calc_offset(coord.x + offset.x, coord.y + offset.y, w, h)


func calc_coordinates(p: int, w: int) -> Vector2i:
	@warning_ignore("integer_division")
	return Vector2i(p % w, p / w)


func calc_offset(x: int, y: int, w: int, h: int) -> int:
	if x < 0 or y < 0 or x >= w or y >= h:
		return -1
	return (x % w) + (y * w)
